<?php
/**
 * @file
 * block_settings.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function block_settings_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'footer address block';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_address_block';
  $fe_block_boxes->body = '<div class="footer-logo"><a href="/"><img alt="" src="/sites/default/files/DM_white_logo.svg" /></a></div>

<div class="footer-address">177 Main Street (Route 27)<br />
Acton, Massachusetts 01720<br />
Phone: 978-264-4200<br />
fun@discoverymuseums.org&nbsp;</div>

<div class="footer-copyright">©[date:custom:Y]&nbsp;The Discovery Museums I <a href="http://www.theoryonedesign.com" target="_blank">Site Credits</a></div>

<div class="organization">The Discovery Museums are an independent 501(c)(3) nonprofit organization.</div>
';

  $export['footer_address_block'] = $fe_block_boxes;

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Inside: Discovery Museum';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'inside_discovery_museum';
  $fe_block_boxes->body = '<figure class="image"><img alt="" src="/sites/default/files/DM_inside_icon.svg" style="width: 329px; height: 275px;" />
<figcaption><a href="#children"><i>Indoors:</i> Discovery Museum</a></figcaption>
</figure>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce tincidunt pharetra erat, et accumsan felis convallis id. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tincidunt maximus elit, eget interdum lectus scelerisque ac.</p>

<p>&nbsp;</p>

<p><a href="#museum">Discovery Museum Exhibits</a></p>
';

  $export['inside_discovery_museum'] = $fe_block_boxes;

  return $export;
}
