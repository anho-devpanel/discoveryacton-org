<?php
/**
 * @file
 * block_settings.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function block_settings_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-inside_discovery_museum'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'inside_discovery_museum',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'nexus' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'nexus',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
