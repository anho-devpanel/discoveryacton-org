<?php
/**
 * @file
 * block_settings.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function block_settings_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_block_image'.
  $field_bases['field_block_image'] = array(
    'active' => 1,
    'cardinality' => 4,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_block_image',
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  return $field_bases;
}
