<?php
/**
 * @file
 * panel_settings.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function panel_settings_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'exhibit';
  $mini->category = '';
  $mini->admin_title = 'Exhibit';
  $mini->admin_description = '';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 4,
          1 => 2,
          2 => 'main-row',
          3 => 3,
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'left_',
          1 => 'center',
          2 => 'right_',
        ),
        'parent' => 'main',
        'class' => 'exhibit-overview-section',
        'hide_empty' => 0,
      ),
      'center' => array(
        'type' => 'region',
        'title' => 'Center',
        'width' => '32.3857880087692',
        'width_type' => '%',
        'parent' => 'main-row',
      ),
      2 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'left__',
          1 => 'center_',
          2 => 'right',
        ),
        'parent' => 'main',
        'class' => '',
        'hide_empty' => 0,
      ),
      'right' => array(
        'type' => 'region',
        'title' => 'right',
        'width' => '22.09144615474608',
        'width_type' => '%',
        'parent' => '2',
        'class' => '',
        'hide_empty' => 0,
      ),
      'center_' => array(
        'type' => 'region',
        'title' => 'center',
        'width' => '20.68177018972001',
        'width_type' => '%',
        'parent' => '2',
        'class' => '',
        'hide_empty' => 0,
      ),
      'left_' => array(
        'type' => 'region',
        'title' => 'left',
        'width' => '34.23906942846807',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
        'hide_empty' => 0,
      ),
      'right_' => array(
        'type' => 'region',
        'title' => 'right',
        'width' => '33.37514256276273',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
        'hide_empty' => 0,
      ),
      'left__' => array(
        'type' => 'region',
        'title' => 'left',
        'width' => '22.36128658197481',
        'width_type' => '%',
        'parent' => '2',
        'class' => '',
        'hide_empty' => 0,
      ),
      3 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center__',
        ),
        'parent' => 'main',
        'class' => '',
        'hide_empty' => 0,
      ),
      'center__' => array(
        'type' => 'region',
        'title' => 'center',
        'width' => 75,
        'width_type' => '%',
        'parent' => '3',
        'class' => '',
        'hide_empty' => 0,
      ),
      4 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'center___',
        ),
        'parent' => 'main',
        'class' => '',
        'hide_empty' => 0,
      ),
      'center___' => array(
        'type' => 'region',
        'title' => 'center',
        'width' => 100,
        'width_type' => '%',
        'parent' => '4',
        'class' => '',
        'hide_empty' => 0,
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'right' => NULL,
      'center_' => NULL,
      'left_' => NULL,
      'right_' => NULL,
      'left__' => NULL,
      'right__' => NULL,
      'center__' => NULL,
      'left___' => NULL,
      'center___' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'd1955574-78e1-4155-a9b9-3100f74e0cb5';
  $display->storage_type = 'panels_mini';
  $display->storage_id = 'exhibit';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-9eabc629-771e-468b-961d-52953998ed51';
  $pane->panel = 'center';
  $pane->type = 'block';
  $pane->subtype = 'block-2';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => 'Science discovery museum ',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '9eabc629-771e-468b-961d-52953998ed51';
  $display->content['new-9eabc629-771e-468b-961d-52953998ed51'] = $pane;
  $display->panels['center'][0] = 'new-9eabc629-771e-468b-961d-52953998ed51';
  $pane = new stdClass();
  $pane->pid = 'new-c82805d2-762f-4dfc-ab2c-5c4f51813b8c';
  $pane->panel = 'center___';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => '',
    'title' => '',
    'body' => '<p class="rtecenter">exhibits</p>
',
    'format' => 'full_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c82805d2-762f-4dfc-ab2c-5c4f51813b8c';
  $display->content['new-c82805d2-762f-4dfc-ab2c-5c4f51813b8c'] = $pane;
  $display->panels['center___'][0] = 'new-c82805d2-762f-4dfc-ab2c-5c4f51813b8c';
  $pane = new stdClass();
  $pane->pid = 'new-de5e4aad-f3da-409e-a170-ccb6be741879';
  $pane->panel = 'left_';
  $pane->type = 'block';
  $pane->subtype = 'block-50';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'de5e4aad-f3da-409e-a170-ccb6be741879';
  $display->content['new-de5e4aad-f3da-409e-a170-ccb6be741879'] = $pane;
  $display->panels['left_'][0] = 'new-de5e4aad-f3da-409e-a170-ccb6be741879';
  $pane = new stdClass();
  $pane->pid = 'new-ea8800b3-f142-4341-a6ae-33b38ca1518c';
  $pane->panel = 'left_';
  $pane->type = 'block';
  $pane->subtype = 'block-3';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => 'Children\'s discovery museum',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'ea8800b3-f142-4341-a6ae-33b38ca1518c';
  $display->content['new-ea8800b3-f142-4341-a6ae-33b38ca1518c'] = $pane;
  $display->panels['left_'][1] = 'new-ea8800b3-f142-4341-a6ae-33b38ca1518c';
  $pane = new stdClass();
  $pane->pid = 'new-96899227-27ab-4e62-b025-7b3566be2616';
  $pane->panel = 'right_';
  $pane->type = 'block';
  $pane->subtype = 'block-29';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => 'Discovery woods',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '96899227-27ab-4e62-b025-7b3566be2616';
  $display->content['new-96899227-27ab-4e62-b025-7b3566be2616'] = $pane;
  $display->panels['right_'][0] = 'new-96899227-27ab-4e62-b025-7b3566be2616';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $mini->display = $display;
  $export['exhibit'] = $mini;

  return $export;
}
