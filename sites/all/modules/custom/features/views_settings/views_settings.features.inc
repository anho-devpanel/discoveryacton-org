<?php
/**
 * @file
 * views_settings.features.inc
 */

/**
 * Implements hook_views_api().
 */
function views_settings_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
