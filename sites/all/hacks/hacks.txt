For each hack you do to core or a contributed module, do this:
1. Create a patch. (Details here: http://portal.dl-dev.com/documentation/how-create-patch) Name it like this: project-version.patch, e.g. date_7.x-2.8.patch or drupal_7.38.patch
2. Move the patch into this directory (sites/all/hacks)
3. Make an entry below for it, where you give its name and what it's accomplishing.

Sample entry:
Drupal (drupal 7.37.patch)
Hacking Drupal to address the mysterious appearance of this error from NuoDB, sometimes when enabling a module, sometimes when creating a new content type: "Illegal null in field description in table menu_router..."

Real entries:

IMCE (pantheon-2962030-15.patch)
IMCE browser was very slow after migrating the site to Pantheon - https://www.drupal.org/project/imce/issues/2962030.

CKEditor (ckeditor-load-issue.patch)
After the module was updated from 7.x-1.17 to 7.x-1.19, the editor stopped loading by default. The patch addresses the issue.
The issue occurred updating to 7.x-1.18 as well.
