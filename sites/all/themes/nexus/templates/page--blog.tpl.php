<?php

?>
<a href="#primary" class="skip-link">Skip to Content</a>
<div id="page">

	<div class="blog-section"><?php print render ($page['blog']); ?></div>

  <header id="masthead" class="site-header" role="banner">
	  <div class="container">
    <div class="row">
      <div id="logo" class="site-branding col-sm-4">
        <?php if ($logo): ?><div id="site-logo"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="/sites/all/themes/nexus/images/DM_color_logo.svg" alt="<?php print t('Home'); ?>" />
        </a></div><?php endif; ?>
         <!--<h1 id="site-title">
          <a href="<?php //print $front_page; ?>" title="<?php //print t('Home'); ?>"><?php //print $site_name; ?>home</a>
        </h1>-->
      </div>
      <div class="blog-link col-sm-2"><a href="#"><span>Play Matters</span></a></div>
      <div class="col-sm-8 mainmenu">
        <div class="mobilenavi"></div>
        <nav id="navigation" role="navigation">
          <div id="main-menu">
            <?php
              if (module_exists('i18n_menu')) {
                $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
              } else {
                $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
              }
              print drupal_render($main_menu_tree);
            ?>
          </div>
        </nav>
      </div>
            <div class="secondary-menu col-sm-6"> <?php print render ($page['secondary_menu']); ?></div>
            <div class="search col-sm-2"><?php //print render ($page['search']); ?>

					<?php   global $base_path;
						print "<img alt='search' src='" . $base_path . path_to_theme() . "/images/svg_images/search icon.svg' />";?> </div>
                <div class="search-main">
               <div class="container">
                  <div class="search-block col-sm-3">
			 		  <?php $block = module_invoke('search', 'block_view', 'search');
						print render($block);
						?>
				  </div>
				  </div>
				  </div>

    </div>
    </div>
  </header>

  <div id="banner-block">

    <div class="row">
      <div class="col-sm-12">
        <?php print render($page['banner']); ?>
      </div>
    </div>

  </div>

    <div id="main-content">
    <div class="container">
      <div class="row">
      <div id="primary" class="content-area col-sm-12; ?>">
          <section id="content" role="main" class="clearfix">
            <?php //print $messages; ?>
            <?php if ($page['content_top']): ?><div id="content_top"><?php print render($page['content_top']); ?></div><?php endif; ?>

            <div id="content-wrap">
              <?php print render($title_prefix); ?>
              <?php if ($title): ?><h1 class="page-title"><?php print $title; ?></h1><?php endif; ?>
              <?php print render($title_suffix); ?>
              <?php if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper clearfix"><?php print render($tabs); ?></div><?php endif; ?>
              <?php print render($page['help']); ?>
              <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
              <?php print render($page['content']); ?>
            </div>
          </section>
        </div>

      </div>
    </div>
  </div>

  <?php if($page['footer']) : ?>
    <div id="footer-block">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <?php print render($page['footer']); ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if ($page['footer_first'] || $page['footer_second'] || $page['footer_third'] || $page['footer_fourth']): ?>
    <?php $footer_col = ( 12 / ( (bool) $page['footer_first'] + (bool) $page['footer_second'] + (bool) $page['footer_third'] + (bool) $page['footer_fourth'] ) ); ?>
    <div id="bottom">
      <div class="container">
        <div class="row">
          <?php if($page['footer_first']): ?><div class="footer-block footer-first col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_first']); ?>
          </div><?php endif; ?>
          <?php if($page['footer_second']): ?><div class="footer-block footer-second col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_second']); ?>
          </div><?php endif; ?>
          <?php if($page['footer_third']): ?><div class="footer-block footer-thired col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_third']); ?>
          </div><?php endif; ?>
          <?php if($page['footer_fourth']): ?><div class="footer-block footer-forth col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_fourth']); ?>
          </div><?php endif; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>



<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-575acf4fd97d2e99"></script>


