<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

?>
<?php foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>
  <?php print $field->wrapper_prefix; ?>
    <?php if (!empty($row->_field_data['nid']['entity']->type == 'today_s_hours')): 
	$mh = $row->_field_data['nid']['entity']->field_science_discovery_museum_h['und'][0]['value'];
    	$ot = $row->_field_data['nid']['entity']->field_override_normal_times_['und'][0]['value'];
    	$otc = ($ot === "yes") ? "override-times-yes" : "override-times-no";
	    if ($mh !== "CLOSED") {
    ?>
      <div class="open-schedule-header <?php print $otc; ?>"><h3>OPEN</h3><span class="open-schedule"><?php print $closed_schedule = $row->_field_data['nid']['entity']->field_science_discovery_museum_h['und'][0]['value']; ?></span></div>    
	<?php   } else { ?>
	<div class="open-schedule-header <?php print $otc; ?>"><h3>CLOSED</h3></div>
	<?php } elseif ($row->node_type == 'event' && $row->node_title == 'No staff-led programs'):?>
	  <h3><?php print t('No staff-led programs');?></h3>
	<?php else: ?>  
      <?php print $field->label_html; ?>
      <?php print $field->content; ?>
    <?php endif; ?>  
  <?php print $field->wrapper_suffix; ?>
<?php endforeach; ?>