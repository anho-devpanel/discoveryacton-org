<a href="#primary" class="skip-link">Skip to Content</a>
<div id="page">

	<div class="blog-section"><?php print render ($page['blog']); ?></div>

  <header id="masthead" class="site-header" role="banner">
	  <div class="container">
    <div class="row">
      <div id="logo" class="site-branding col-sm-4">
        <?php if ($logo): ?><div id="site-logo"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="/sites/all/themes/nexus/images/DM_color_logo.svg" alt="<?php print t('Home'); ?>" />
        </a></div><?php endif; ?>
         <!--<h1 id="site-title">
          <a href="<?php //print $front_page; ?>" title="<?php //print t('Home'); ?>"><?php //print $site_name; ?>home</a>
        </h1>-->
      </div>
      <div class="blog-link col-sm-2"><a href="#"><span>Play Matters</span></a></div>
      <div class="col-sm-8 mainmenu">
        <div class="mobilenavi"></div>
        <nav id="navigation" role="navigation">
          <div id="main-menu">
            <?php
              if (module_exists('i18n_menu')) {
                $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
              } else {
                $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu'));
              }
              print drupal_render($main_menu_tree);
            ?>
          </div>
        </nav>
      </div>
            <div class="secondary-menu col-sm-6"> <?php print render ($page['secondary_menu']); ?></div>
            <div class="search col-sm-2"><?php //print render ($page['search']); ?>

					<?php   global $base_path;
						print "<img alt='search' src='" . $base_path . path_to_theme() . "/images/svg_images/search icon.svg' />"; ?> </div>
                 <div class="search-main">
               <div class="container">
                  <div class="search-block col-sm-3">
			 		  <?php $block = module_invoke('search', 'block_view', 'search');
						print render($block);
						?>
				  </div>
				  </div>
				  </div>


    </div>
    </div>
  </header>

  <div id="banner-block">

    <div class="row">
      <div class="col-sm-12">
        <?php print render($page['banner']); ?>
      </div>
    </div>

  </div>

  <?php if ($is_front): ?>
  <?php if (theme_get_setting('slideshow_display','nexus')): ?>
  <?php
    $slide1_head = check_plain(theme_get_setting('slide1_head','nexus'));   $slide1_desc = check_markup(theme_get_setting('slide1_desc','nexus'), 'full_html'); $slide1_url = check_plain(theme_get_setting('slide1_url','nexus'));
    $slide2_head = check_plain(theme_get_setting('slide2_head','nexus'));   $slide2_desc = check_markup(theme_get_setting('slide2_desc','nexus'), 'full_html'); $slide2_url = check_plain(theme_get_setting('slide2_url','nexus'));
    $slide3_head = check_plain(theme_get_setting('slide3_head','nexus'));   $slide3_desc = check_markup(theme_get_setting('slide3_desc','nexus'), 'full_html'); $slide3_url = check_plain(theme_get_setting('slide3_url','nexus'));
  ?>
  <div id="slidebox" class="flexslider">
    <ul class="slides">
      <li>
        <img src="<?php print base_path() . drupal_get_path('theme', 'nexus') . '/images/slide-image-1.jpg'; ?>"/>
        <?php if($slide1_head || $slide1_desc) : ?>
          <div class="flex-caption">
            <h2><?php print $slide1_head; ?></h2><?php print $slide1_desc; ?>
            <a class="frmore" href="<?php print url($slide1_url); ?>"> <?php print t('READ MORE'); ?> </a>
          </div>
        <?php endif; ?>
      </li>
      <li>
        <img src="<?php print base_path() . drupal_get_path('theme', 'nexus') . '/images/slide-image-2.jpg'; ?>"/>
        <?php if($slide2_head || $slide2_desc) : ?>
          <div class="flex-caption">
            <h2><?php print $slide2_head; ?></h2><?php print $slide2_desc; ?>
            <a class="frmore" href="<?php print url($slide2_url); ?>"> <?php print t('READ MORE'); ?> </a>
          </div>
        <?php endif; ?>
      </li>
      <li>
        <img src="<?php print base_path() . drupal_get_path('theme', 'nexus') . '/images/slide-image-3.jpg'; ?>"/>
        <?php if($slide3_head || $slide3_desc) : ?>
          <div class="flex-caption">
            <h2><?php print $slide3_head; ?></h2><?php print $slide3_desc; ?>
            <a class="frmore" href="<?php print url($slide3_url); ?>"> <?php print t('READ MORE'); ?> </a>
          </div>
        <?php endif; ?>
      </li>
    </ul><!-- /slides -->
    <div class="doverlay"></div>
  </div>
  <?php endif; ?>
  <?php endif; ?>

 <?php if($page['header']) : ?>
    <div id="header-block">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <?php print render($page['header']); ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>



    <div id="main-content">
    <div class="container">
      <div class="row">
        <?php if($page['sidebar_first']) { $primary_col = 6; } else { $primary_col = 9; } ?>

        <?php if ($page['left_sidebar']): ?>
        <div class="sidebar_left col-sm-3"><?php //print render($page['left_sidebar']); ?></div><?php endif; ?>

        <div id="primary" class="content-area col-sm-<?php print $primary_col; ?>">
          <section id="content" role="main" class="clearfix">
            <?php //print $messages; ?>
            <?php if ($page['content_top']): ?><div id="content_top"><?php print render($page['content_top']); ?></div><?php endif; ?>

            <div id="content-wrap">

              <?php if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper clearfix"><?php print render($tabs); ?></div><?php endif; ?>
              <?php print render($page['help']); ?>
              <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
              <?php print render($page['content']); ?>
            </div>
          </section>
        </div>

        <?php if ($page['sidebar_first']): ?>
          <aside id="sidebar" class="col-sm-3" role="complementary">
           <?php print render($page['sidebar_first']); ?>
          </aside>
        <?php endif; ?>
      </div>
    </div>
  </div>

<?php if($page['calendar_section']) : ?>

 <div class="exhibit-section">
	  <div class="container">
	   <?php print render($page['calendar_section']); ?>
	   </div>
  </div>

<?php endif; ?>



<?php if($page['teacher_section']) : ?>

 <div class="teacher-section">
	  <div class="container">
	   <?php print render($page['teacher_section']); ?>
	   </div>
  </div>

<?php endif; ?>

<?php if($page['blog_section']) : ?>

  <div id="museum" class="children-section">
      <div class="container">
      <?php print render($page['blog_section']); ?>
      </div>
   </div>
<?php endif; ?>

<?php if($page['news_section']) : ?>

  <div id="science-section" class="science-section">
      <div class="container">
      <?php print render($page['news_section']); ?>
      </div>
  </div>
<?php endif; ?>

  <?php if($page['footer']) : ?>
    <div id="wood-block">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <?php print render($page['footer']); ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if ($page['footer_first'] || $page['footer_second'] || $page['footer_third'] || $page['footer_fourth']): ?>
    <?php $footer_col = ( 12 / ( (bool) $page['footer_first'] + (bool) $page['footer_second'] + (bool) $page['footer_third'] + (bool) $page['footer_fourth'] ) ); ?>
    <div id="bottom">
      <div class="container">
        <div class="row">
          <?php if($page['footer_first']): ?><div class="footer-block footer-first col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_first']); ?>
          </div><?php endif; ?>
          <?php if($page['footer_second']): ?><div class="footer-block footer-second col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_second']); ?>
          </div><?php endif; ?>
          <?php if($page['footer_third']): ?><div class="footer-block footer-thired col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_third']); ?>
          </div><?php endif; ?>
          <?php if($page['footer_fourth']): ?><div class="footer-block footer-forth col-sm-<?php print $footer_col; ?>">
            <?php print render ($page['footer_fourth']); ?>
          </div><?php endif; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>


<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-575acf4fd97d2e99"></script>

