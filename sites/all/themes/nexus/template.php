<?php
/**
 * Implements hook_html_head_alter().
 * This will overwrite the default meta character type tag with HTML5 version.
 */
function nexus_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8'
  );
}

/**
 * Insert themed breadcrumb page navigation at top of the node content.
 */
 
/*function nexus_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Adding the title of the current page to the breadcrumb.
    $breadcrumb[] = drupal_get_title();
        $output = '' . t('You are here') . '';
        $output .= '' . implode(' » ', $breadcrumb);
       
    return $output;
  }
  
  $breadcrumb = $variables['breadcrumb'];
  $crumbs = '';
  if (!empty($breadcrumb)) {
    $crumbs = '<div id="breadcrumbs"><span>You are here: </span><ul>';
    foreach($breadcrumb as $value) {
      $crumbs .= '<li>' . $value . '</li>';
    }
    $crumbs .= '<li>' . drupal_get_title() . '</li></ul></div>';
  }
  return $crumbs;
}
*/

//~ function nexus_breadcrumb($variables) {
  //~ $breadcrumb = $variables['breadcrumb'];
  //~ if (!empty($breadcrumb)) {
    //~ // Use CSS to hide titile .element-invisible.
    //~ $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    //~ // comment below line to hide current page to breadcrumb
//~ $breadcrumb[] = drupal_get_title();
    //~ $output .= '<nav class="breadcrumb">' . implode(' <span class="seperator"> | </span> ', $breadcrumb) . '</nav>';
    //~ return $output;
  //~ }
//~ }


/**
 * Override or insert variables into the page template.
 */
function nexus_preprocess_page(&$vars) {
  if (isset($vars['main_menu'])) {
    $vars['main_menu'] = theme('links__system_main_menu', array(
      'links' => $vars['main_menu'],
      'attributes' => array(
        'class' => array('links', 'main-menu', 'clearfix'),
      ),
      'heading' => array(
        'text' => t('Main menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      )
    ));
  }
  else {
    $vars['main_menu'] = FALSE;
  }
  if (isset($vars['secondary_menu'])) {
    $vars['secondary_menu'] = theme('links__system_secondary_menu', array(
      'links' => $vars['secondary_menu'],
      'attributes' => array(
        'class' => array('links', 'secondary-menu', 'clearfix'),
      ),
      'heading' => array(
        'text' => t('Secondary menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      )
    ));
  }
  else {
    $vars['secondary_menu'] = FALSE;
  }
if (isset($vars['node'])) {
    // If the node type is "blog_madness" the template suggestion will be "page--blog-madness.tpl.php".
    $vars['theme_hook_suggestions'][] = 'page__'. $vars['node']->type;
  }
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
 */
function nexus_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}

/**
 * Override or insert variables into the node template.
 */
function nexus_preprocess_node(&$variables) {
  $node = $variables['node'];
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
  $variables['date'] = t('!datetime', array('!datetime' =>  date('j F Y', $variables['created'])));
}

function nexus_page_alter($page) {
  // <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  $viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
    'name' =>  'viewport',
    'content' =>  'width=device-width, initial-scale=1, maximum-scale=1'
    )
  );
  drupal_add_html_head($viewport, 'viewport');
}


/**
 * Add javascript files for front-page jquery slideshow.
 */
if (drupal_is_front_page()) {
  drupal_add_js(drupal_get_path('theme', 'nexus') . '/js/jquery.flexslider.js');
  drupal_add_js(drupal_get_path('theme', 'nexus') . '/js/slide.js');
}

function nexus_preprocess_calendar_datebox(&$vars) {
  $date = $vars['date'];
  $view = $vars['view'];
  $day_path = calendar_granularity_path($view, 'day');
  $vars['url'] = 'calendar/day/' . $date;
  $vars['link'] = !empty($day_path) ?l($vars['day'],$vars['url']):$vars['day']; 
}

/**
 * Implements hook_views_pre_render()
 */
function nexus_views_pre_render(&$view) {
  if ($view->name == 'calender' && $view->current_display == 'page_1') {

    if (!empty($view->result)) {
		foreach($view->result as $key => $data) {
		  $content_types[] = $data->node_type;
		}
		// If no event content, add empty data to display
		if (!in_array('event', $content_types)) {
		  $field_data = array(
			'node_type' => 'event',
			'node_title' => t('No staff-led programs'),
		  );
		}
		// If no today's hour content, add empty data to display
		if (!in_array('today_s_hours', $content_types)) {
		  $field_data = array(
			'node_type' => 'today_s_hours',
			'node_title' => t('CLOSE'),
		  );
		}
        // Set resutl data and merge
		$field_data = (object) $field_data;
		$field_data_set = array($field_data);
		$updated_result = array_merge($view->result, $field_data_set);
        // Cleanup result, remove empty field rows
		foreach ($updated_result as $result_data) {
          if (!empty($result_data->node_title)) {
            $result[] = $result_data;
		  }
		}
        // Override result
		$view->result = $result;
	}
  }
  // Calendar week views page
  if ($view->name == 'calender' && $view->current_display == 'page_2') {
    // Collect dates
	foreach($view->result as $key => $data)  {
	  if (!empty($data->field_data_field_event_field_event_value)) {
        $date_set[] = date('Y-m-d', strtotime($data->field_data_field_event_field_event_value));
	  }
	  else {
        $date_set[] = date('Y-m-d', strtotime($data->field_data_field_today_s_date_field_today_s_date_value));
	  }
	}
	$date_set = array_unique($date_set);

	// Get default days of the week
	$week_filter = explode('-', $view->args[0]);
	$week = str_replace('W', '', $week_filter[1]);
	$week = (int) $week;
	$year = $week_filter[0];
	$week_array = nexus_get_start_end_date($year, $week);
	$default_week_days = nexus_get_date_list_of_week($week_array['week_start'], $week_array['week_end']);

	// Set missing dates
	$empty_result_dates = array_diff($default_week_days, $date_set);

	// Alter result if there are empty dates
	if (!empty($empty_result_dates )) {
      foreach ($empty_result_dates as $empty_date) {
        $field_data = array(
          'field_data_field_today_s_date_field_today_s_date_value' => $empty_date . ' 00:00:00',
          'field_field_today_s_date' => array(
            array(
              'rendered' => array(
              '#markup' => date('l, F j', strtotime($empty_date)),
              '#access' => TRUE,
            ),
            'raw' => array(
              'value' => $empty_date . ' 00:00:00',
              'rrule' => '',
              'timezone' => 'America/New_York',
              'timezone_db' => 'America/New_York',
              'date_type' => 'datetime',
            ),
            ),
          ),
        );
        $empty_date_set[] = (object) $field_data;
      }

      $view->result = array_merge($view->result, $empty_date_set);

	  // Assign empty events
      foreach($view->result as $data) {
        if (!empty($data->field_data_field_event_field_event_value)) {
		  $updated_date_set[] = date('Y-m-d', strtotime($data->field_data_field_event_field_event_value));
		  $with_event_dates[] = date('Y-m-d', strtotime($data->field_data_field_event_field_event_value));
        }
        else {
		  $updated_date_set[] = date('Y-m-d', strtotime($data->field_data_field_today_s_date_field_today_s_date_value));
		  if (empty($data->node_title)) {
		    $with_event_dates[] = date('Y-m-d', strtotime($data->field_data_field_today_s_date_field_today_s_date_value));
		  }
        } 
      }
	  $updated_date_set = array_unique($updated_date_set);
	  $with_event_dates = array_unique($with_event_dates);
	  // Set dummy date for emmpty events
      foreach ($updated_date_set as $updated_date) {
		if (!in_array($updated_date, $with_event_dates)) {
          $event_field_data = array(
		    'node_type' => 'event',
		    'node_title' => 'No staff-led programs',
		    'field_data_field_today_s_date_field_today_s_date_value' => $updated_date . ' 00:00:00',
			  'field_field_today_s_date' => array(
				array(
				  'rendered' => array(
				  '#markup' => date('l, F j', strtotime($updated_date)),
				  '#access' => TRUE,
				),
				'raw' => array(
				  'value' => $updated_date . ' 00:00:00',
				  'rrule' => '',
				  'timezone' => 'America/New_York',
				  'timezone_db' => 'America/New_York',
				  'date_type' => 'datetime',
				),
				),
			  ),
		  );
          $none_event_set[] = (object) $event_field_data;
		}
	  }

	  if (!empty($none_event_set)) {
        // Merge empty events to result
        $view->result = array_merge($view->result, $none_event_set);		   
	  }

	  // Sort result by date
	  $view->result = nexus_sort_custom_result($view->result);

	}
	else {
      $view->result = nexus_sort_custom_result($view->result);
	}
  }
}

/**
 * Get start and end date of a week.
 */
function nexus_get_start_end_date($year, $week) {
  $dto = new DateTime();
  $dto->setISODate($year, $week, 0);
  $set['week_start'] = $dto->format('Y-m-d');
  $dto->modify('+6 days');
  $set['week_end'] = $dto->format('Y-m-d');

  return $set;
}

/**
 * Get list of dates between a week.
 */
function nexus_get_date_list_of_week($start_date, $end_date) {
  return array_map(function($arg) {
    return date('Y-m-d', $arg);
  }, range(strtotime($start_date), strtotime($end_date), 86400));
}

/**
 * Get sort result.
 */
function nexus_sort_custom_result($result) {
  // Get date and sort
  foreach($result as $key => $row) {
    $row_date = (!empty($row->field_data_field_event_field_event_value)) ? $row->field_data_field_event_field_event_value : $row->field_data_field_today_s_date_field_today_s_date_value;
	$date_sort_set[$key] = $row_date;
  }
  uasort($date_sort_set, 'date_sort');
 
  // Set new order for result
  foreach($date_sort_set as $date_key => $date_value) {
    $sorted_result[] = $result[$date_key];
  }

  return $sorted_result;
}

/**
 * Sort date set.
 */
function date_sort($a, $b) {
  return strtotime($a) - strtotime($b);
}
