CKEDITOR.addStylesSet( 'drupal',
[
	/* Block Styles */

	{ name : 'Banner text',
		element : 'h2',
		attributes : { 'class' : 'bluebar' }
	},

  { name : 'Small button',
    element : 'a',
    attributes : { 'class' : 'button-small' }
  }

]);
