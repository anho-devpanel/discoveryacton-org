jQuery(window).load(function() {
  // "display-modal" class is added via the block UI
  if (jQuery("body .display-modal").length > 0) {
    if (!sessionStorage.alreadyClicked) {
      // The modal region is being hidden until the following class is added to the body
      jQuery("body").addClass("alert-modal");
      if (jQuery("body.alert-modal").length > 0) {
        jQuery("body.alert-modal .message-header > h2").focus();
      }
    }
  }
});

// Close when clicking the X button
jQuery(function() {
  jQuery("body #page-alert-dismiss").click(function() {
    jQuery("body").removeClass("alert-modal");
    // Set a session storage value to prevent the modal from reappearing after being closed
    // if the user revisits the homepage. It should only reload if the user closes their tab
    // and comes back later
    sessionStorage.alreadyClicked = 1;
  });
});

// Close when clicking the escape key
jQuery(document).on("keydown", function(e) {
    if ( e.keyCode === 27 ) { // ESC
      jQuery("body").removeClass("alert-modal");
    }
});


