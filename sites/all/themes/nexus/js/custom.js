jQuery(window).load(function() {
  //~ jQuery(".loader").fadeOut(3000);
  jQuery(".blog-link a").click(function() {
    jQuery(".blog-section").toggleClass("open");
  });

  jQuery(".content-area fieldset.collapsible .fieldset-legend a").click(function() {
    jQuery(this).toggleClass("on");
  });

  /* Navigation */

  jQuery('#main-menu > ul').superfish({
    delay: 500, // 0.1 second delay on mouseout
    animation: {
      opacity: 'show',
      height: 'show'
    }, // fade-in and slide-down animation
    dropShadows: true // disable drop shadows
  });

  jQuery('#main-menu > ul').mobileMenu({
    prependTo: '.mobilenavi'
  });

  /* Calendar Mini Widget Empty Dates */
  if (jQuery('#block-views-calender-block-6').length > 0) {
    set_empty_date('#block-views-calender-block-6 .mini-day-off');
  }

  if (jQuery('#block-views-calender-block-7').length > 0) {
    set_empty_date('#block-views-calender-block-7 .mini-day-off');
  }

  function set_empty_date(block) {
    jQuery(block).each(function() {
      var date_value = jQuery(this).parent().attr('id');
      var date_value_set = date_value.split('-');
      var link = '<a href="/calendar/day/' + date_value_set[1] + '-' + date_value_set[2] + '-' + date_value_set[3] + '">' + jQuery(this).text() + '</a>';
      jQuery(this).text('').append(link);
    });
  }

  /* get week of selected date from minical */

  function y2k(number) {
    return (number < 1000) ? number + 1900 : number;
  }

  function getWeek(year, month, day) {
    var when = new Date(year, month, day);
    var newYear = new Date(year, 0, 1);
    var offset = 7 + 0 - newYear.getDay();
    if (offset == 7) offset = 0;
    var daynum = ((Date.UTC(y2k(year), when.getMonth(), when.getDate(), 0, 0, 0) - Date.UTC(y2k(year), 0, 1, 0, 0, 0)) / 1000 / 60 / 60 / 24) + 1;
    var weeknum = Math.floor((daynum - offset + 7) / 7);

    if (weeknum == 0) {
      year--;
      var prevNewYear = new Date(year, 0, 1);
      var prevOffset = 7 + 1 - prevNewYear.getDay();
      if (prevOffset == 2 || prevOffset == 8) weeknum = 53;
      else weeknum = 52;
    }
    return weeknum;
  }
  var url = jQuery(location).attr('href');
  var date = url.split('/');
  if (date[3] == "calendar" && date[4] == "day") {
    var thisdate = date[date.length - 1];

    var now = new Date(thisdate);
    var dateweek = getWeek(y2k(now.getYear()), now.getMonth(), now.getDate());
    var myyear = (y2k(now.getYear()));
    var result = parseInt(dateweek);


    //~ jQuery(".week").attr("href", '/calendar/week/'+myyear+"-W"+result);
    jQuery('.active-date').parent().addClass('include-week');
    var weeklink = jQuery('.include-week td:first-child a').attr('href');
    jQuery(".week").attr("href", weeklink);
  }
  /* get week of selected date from minical end*/


    var ths = jQuery(".open-schedule-header");
    var overriden_ths = [];
    ths.each(function(i, th) {
    	if ( jQuery(th).hasClass("override-times-yes")) overriden_ths.push(jQuery(th).find("h2").text());
    });
    ths.each(function(i, th) {
	if ( jQuery(th).hasClass("override-times-yes"))	return;
	var h2 = jQuery(th).find("h2").text();
	if ( overriden_ths.includes(h2) ) {
	    jQuery(th).parent().parent().remove();
	}
    });

    ct = 1;
    var rows = jQuery(".page-calendar-week #primary .view-calender .views-row");
    rows.each(function(i, el) {
	var $el = jQuery(el);
        $el.removeClass("views-row-1").removeClass("views-row-2");
	if ($el.find(".open-schedule-header").length === 1) {
    	    $el.addClass("views-row-1");
    	    ct = 2;
	}
	else {
    	    $el.addClass("views-row-" + ct);
    	ct++;
	}
    });

   for (var i = 0; i < rows.length; i++ ) {
    var $el = jQuery(rows[i]);
    var type = function(el) { return el.find(".open-schedule-header").length === 1 ? "ths" : "event"};
    var addNoStaff = function(el) { el.find(".open-schedule-header").after("<div class='views-row no-events views-row-2'><h3>No staff-led programs</h3></div>"); };
    if ( type($el) === "ths" ) {
        if ( $el.find(".open-schedule-header h3").text() === "CLOSED" ) continue;
        //console.log($el.find(".open-schedule-header").text() + ' ' + i);
        if ( i == rows.length -1 ) {
            addNoStaff($el);
        }
        else {
            var $next = jQuery(rows[i + 1]);
            if ( type($next) === "ths" ) {
                addNoStaff($el);
            }
        }
    }
}
});


(function() {
  var is_webkit = navigator.userAgent.toLowerCase().indexOf('webkit') > -1,
    is_opera = navigator.userAgent.toLowerCase().indexOf('opera') > -1,
    is_ie = navigator.userAgent.toLowerCase().indexOf('msie') > -1;

  if ((is_webkit || is_opera || is_ie) && 'undefined' !== typeof(document.getElementById)) {
    var eventMethod = (window.addEventListener) ? 'addEventListener' : 'attachEvent';
    window[eventMethod]('hashchange', function() {
      var element = document.getElementById(location.hash.substring(1));

      if (element) {
        if (!/^(?:a|select|input|button|textarea)$/i.test(element.tagName))
          element.tabIndex = -1;

        element.focus();
      }
    }, false);
  }
})();


jQuery(document).ready(function() {

  /* display loader*/
  //jQuery('<div class="preloads"><img src="/images/loading.gif"></div>').appendTo('#page');
  jQuery(".preloads").fadeOut(2300, function() {
    jQuery(".day-calender .view-content, .day-calender .date-nav-wrapper").fadeIn(1600);
  });
  /* */

  jQuery('#block-views-cloud-tag-block li').each(function() {
    var getClass = jQuery(this).attr("class"),
      lastClass = getClass.substr(getClass.lastIndexOf(' ') + 1);
    jQuery("#block-views-cloud-tag-block li." + lastClass + " a").addClass(lastClass);

  });
  jQuery('.pane-views-cloud-tag-block li').each(function() {
    var getClass = jQuery(this).attr("class"),
      lastClass = getClass.substr(getClass.lastIndexOf(' ') + 1);
    jQuery('.pane-views-cloud-tag-block li.' + lastClass + ' a').addClass(lastClass);

  });

  jQuery('#block-views-cloud-tag-block-1 li').each(function() {
    var getClass = jQuery(this).attr("class"),
      lastClass = getClass.substr(getClass.lastIndexOf(' ') + 1);
    jQuery("#block-views-cloud-tag-block-1 li." + lastClass + " a").addClass(lastClass);
  });

  jQuery('.pane-views-cloud-tag-block-1 li').each(function() {
    var getClass = jQuery(this).attr("class"),
      lastClass = getClass.substr(getClass.lastIndexOf(' ') + 1);
    jQuery(".pane-views-cloud-tag-block-1 li." + lastClass + " a").addClass(lastClass);

  });


  jQuery('#webform-client-form-261 .webform-component--preferred-date .webform-calendar-day-0').attr('src', '/sites/all/themes/nexus/images/svg_images/calendar.svg');


  jQuery("#edit-cc-block-redirect-url-1").attr('maxlength', '200');
  var event_type = jQuery("#edit-field-event-type-tid option:selected").text();
  jQuery(".view-header .event-type").append('<h1 class="page-title">' + event_type + '</h1>');
  /* ******************* */
  var url = jQuery(location).attr('href');
  console.log(url.indexOf('?mini'));
  if (url.indexOf('?mini') !== -1) {
    jQuery(".day ").addClass("active");
  }
  if (url.indexOf('?week') !== -1) {
    jQuery(".week ").addClass("active");
  }



  /* ******************* */



  /* Responsive Sidr JS */
  jQuery("#sidr-0-button").click(function() {
    jQuery('#sidr-0-button').toggleClass('open-menu');
  });

  jQuery('.sidr-class-expanded').prepend('<a style="font-size: 1.1em" href="#" class="mean-expand close"></a>')
  jQuery('.sidr-class-expanded ul').hide();

  jQuery('.mean-expand').click(function() {
    var getClass = jQuery(this).parents('li').attr('class'),
      lastClass = getClass.substr(getClass.lastIndexOf(' ') + 1);
    jQuery('.' + lastClass + '> ul').toggle();
    jQuery('.' + lastClass + '> .mean-expand').toggleClass('close open');
    //alert(lastClass);


  });


  /* Responsive Sidr JS */

  //jQuery('.blog-section').hide(300);
  jQuery('.all-events').hide("slow");

  jQuery('.search').click(function() {
    jQuery('.search-block').toggle("slow");

  });



  jQuery("#edit-keys").attr("placeholder", "search the calendar");
  jQuery("#views-exposed-form-news-page-2 #edit-keys").attr("placeholder", "search newsroom");

  jQuery("#edit-search-block-form--4").attr("placeholder", "Search");
  jQuery("#edit-search-block-form--2").attr("placeholder", "Search");
  //jQuery(".email_signup_textfield").attr("placeholder", "join our email list");
  jQuery(".email_signup_submit").attr('value', 'Go');



  jQuery('.view-display-id-page_1 .date-prev a').text('PREVIOUS DAY');
  jQuery('.view-display-id-page_1 .date-next a').text('NEXT DAY');
  jQuery('.view-display-id-page_2 .date-prev a').text('PREVIOUS WEEK');
  jQuery('.view-display-id-page_2 .date-next a').text('NEXT WEEK');

  /* on click move to another tab */

  jQuery('.hours').click(function() {
    jQuery('.quicktabs-style-basic li:nth-child(4) ').addClass('active');
    jQuery('.quicktabs-style-basic li:nth-child(1) ').removeClass('active');
    jQuery('#quicktabs-tabpage-plan_your_visit-0 ').addClass('quicktabs-hide');
    jQuery('#quicktabs-tabpage-plan_your_visit-3 ').removeClass('quicktabs-hide');
  })

  jQuery('.vacation').click(function() {
    jQuery('.quicktabs-style-basic li:nth-child(1) ').addClass('active');
    jQuery('.quicktabs-style-basic li:nth-child(4) ').removeClass('active');
    jQuery('#quicktabs-tabpage-plan_your_visit-3 ').addClass('quicktabs-hide');
    jQuery('#quicktabs-tabpage-plan_your_visit-0 ').removeClass('quicktabs-hide');
  })

  jQuery('.edit-submit-travelling-science-workshop').click(function() {
    jQuery(fieldset).addClass('collapsed');


  })


  /* */



  /* add class based on url */
  jQuery(function() {
    var parms = jQuery(location).attr('href');
    var splits = parms.split('/');
    var arg = splits[splits.length - 1];
    /* current date   */
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
    /* current date end  */
    /*current week */
    Date.prototype.getWeek = function() {
      var onejan = new Date(this.getFullYear(), 0, 1);
      return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    }

    var weekNumber = (new Date()).getWeek();
    var year = (new Date()).getFullYear();
    var outputweek = year + '-W' + weekNumber;

    /* current week end */

    switch (window.location.pathname) {

      case '/calendar/day/' + arg:
        jQuery('.day').addClass('active');
        jQuery("tbody tr td").removeClass('today');
        break;

      case '/calendar/day':
        jQuery('.day').addClass('active');
        window.location = '/calendar/day/' + output;
        break;

      case '/calendar/week':
        jQuery('.week').addClass('active');
        window.location = '/calendar/week/' + outputweek;

        break;

      case '/calendar/week/' + outputweek:
        jQuery("tbody tr td").has('a[href*="/calendar/week/' + arg + '"]').addClass('today');
        jQuery('.week').addClass('active');
        jQuery('.calendar-calendar tr.active-week td.sun').addClass('today-date');
        break;

      case '/calendar/week/' + arg:
        jQuery('.week').addClass('active');
        jQuery("tbody tr td").removeClass('today');
        jQuery('.calendar-calendar tr.active-week td.sun').addClass('today-week-day');
        jQuery("tbody tr td").removeClass('today');
        break;

    }
  });

  /* after page load add class on (td tr) clicked date */
  var parms = jQuery(location).attr('href');
  var splits = parms.split('/');
  var arg = splits[splits.length - 1];

  jQuery('#calender-' + arg).addClass('active-date');
  // jQuery('#block-views-calender-block-7 td.active-date').parent('tr').addClass('active-week');
  jQuery('#block-views-calender-block-7 tbody tr').has('a[href*="/calendar/day/' + arg + '"]').addClass('active-week');
  jQuery('#block-views-calender-block-7 tbody tr').has('a[href*="/calendar/week/' + arg + '"]').addClass('active-week');
  jQuery('.calendar-calendar tr.active-week td.sun').addClass('today');

  jQuery('.apply').click(function() {
    jQuery('.collapse-processed').addClass('collapsed');
  });


  jQuery("#edit-field-level-3-sponsor-name-und-0-value").attr('maxlength', '500');

  /*  go back link on exhibit page  */
  jQuery('a.back').click(function() {
    parent.history.back();
    return false;
  });

  /*  go back link on exhibit page end  */



});

jQuery(document).ajaxComplete(function() {

  var event_type = jQuery("#edit-field-event-type-tid option:selected").text();
  jQuery(".view-header .event-type").append('<h1 class="page-title">' + event_type + '</h1>');

  jQuery("#edit-field-level-3-sponsor-name-und-0-value").attr('maxlength', '500');
  jQuery('.view-display-id-page_1 .date-prev a').text('PREVIOUS DAY');
  jQuery('.view-display-id-page_1 .date-next a').text('NEXT DAY');
  jQuery('.view-display-id-page_2 .date-prev a').text('PREVIOUS WEEK');
  jQuery('.view-display-id-page_2 .date-next a').text('NEXT WEEK');
  jQuery("#edit-search-block-form--4").attr("placeholder", "Search");
  jQuery("#edit-keys").attr("placeholder", "Search The Calendar");
  jQuery("#views-exposed-form-news-page-2 #edit-keys").attr("placeholder", "Search Newsroom");
  jQuery("#edit-field-ma-ste-standards-tid").before("<input type='button' size=15 value='MA STE FILTER' id='ma-ste' /></br>");
  jQuery('#ma-ste').click(function() {
    jQuery('#edit-field-ma-ste-standards-tid').slideToggle('slow');

  });
  jQuery(function() {

    var r = jQuery('<input type="button" value="Close" class="apply"/>');
    jQuery(".view-travelling-science-workshop .form-item-field-grades-tid .form-checkboxes, .view-travelling-science-workshop .form-item-field-ma-ste-standards-tid .form-checkboxes").append(r);

    jQuery('.apply').click(function() {
      jQuery('.collapse-processed').addClass('collapsed');
    });


  });

});


/* Apply button for dropdown*/
jQuery(function() {

  var r = jQuery('<input type="button" value="Close" class="apply"/>');
  jQuery(".view-travelling-science-workshop .form-item-field-grades-tid .form-checkboxes, .view-travelling-science-workshop .form-item-field-ma-ste-standards-tid .form-checkboxes").append(r);

  jQuery('.apply').click(function() {
    jQuery('.collapse-processed').addClass('collapsed');
  });


});
